package co.com.goldenrace.ticket.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;


import lombok.Data;

@Data
public class ResponseDTO {
	
	private StatusResponse statusResponse;
	
	@JsonInclude(Include.NON_NULL)
	private Object data;

}

package co.com.goldenrace.ticket.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
@Data
public class TicketDTO {
	
	@JsonProperty(value = "Id_ticket")
	private String id_ticket;
	
	@JsonProperty(value = "F_creacion")
	private String f_creacion;
	
	@JsonProperty(value = "Total")
	private String total;
	
	@JsonProperty(value = "Id_detalle")
	private String id_detalle;
	
	@JsonProperty(value = "Descripcion")
	private String descripcion;
	
	@JsonProperty(value = "Cantidad")
	private String cantidad;
	
}

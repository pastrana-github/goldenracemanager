package co.com.goldenrace.ticket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.goldenrace.ticket.dto.ResponseDTO;
import co.com.goldenrace.ticket.service.IDataDbTicketService;


@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("consulta")
public class TicketController {
	@Autowired
	private IDataDbTicketService iDataDbTicketService;
	
	@GetMapping("/identificador/{id}")
	public ResponseEntity<Object> getTicketId(@PathVariable("id") String id) {
		ResponseEntity<ResponseDTO> responseDb = iDataDbTicketService.getTicketId(id);
		return new ResponseEntity<>(responseDb, HttpStatus.OK);
	}
	
	@GetMapping("/rangofechas/inicio:{finicio}/fin:{ffin}")
	public ResponseEntity<Object> getTicketFechas(@PathVariable("finicio") String finicio, 
			@PathVariable("ffin") String ffin) {
		ResponseEntity<ResponseDTO> responseDb = iDataDbTicketService.getTicketFechas(finicio,ffin);
		return new ResponseEntity<>(responseDb.getBody(), HttpStatus.OK);
	}
}

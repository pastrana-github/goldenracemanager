package co.com.goldenrace.ticket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan({"co.com.goldenrace.ticket" })
@SpringBootApplication
public class GoldenRaceManagerConsultasoporteApplication {

	public static void main(String[] args) {
		SpringApplication.run(GoldenRaceManagerConsultasoporteApplication.class, args);
	}

}

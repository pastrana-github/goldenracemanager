package co.com.goldenrace.ticket.service;

import org.springframework.http.ResponseEntity;

import co.com.goldenrace.ticket.dto.ResponseDTO;

public interface IDataDbTicketService {

	ResponseEntity<ResponseDTO> getTicketId(String id);
	ResponseEntity<ResponseDTO> getTicketFechas(String finicio, String ffin);
}

package co.com.goldenrace.ticket.service.impl;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import co.com.goldenrace.ticket.dto.ResponseDTO;
import co.com.goldenrace.ticket.dto.TicketDTO;
import co.com.goldenrace.ticket.response.ResponseBuilder;
import co.com.goldenrace.ticket.service.IDataDbTicketService;

@Service
public class DataDbTicketServiceImpl implements IDataDbTicketService {
	
	private RestTemplate restTemplate= new RestTemplate();
	
	@Value("${app.endpoint.wsRestendpointdataticket}")
	private String endpointDataTicket;
	
	@Override
	public ResponseEntity<ResponseDTO> getTicketId(String id) {		
		JsonNode root = null;
		TicketDTO ticketDTO = new TicketDTO();
		ObjectMapper mapper = new ObjectMapper();
		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(endpointDataTicket + "/goldenrace/ticket/consulta");	
		List<TicketDTO> listTicketDTO = new ArrayList<>();	
		try {
			ResponseEntity<String> responseData = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, null, String.class);
			String responseBody = responseData.getBody();
			root = mapper.readTree(responseBody);
			for (JsonNode jsonNode : root) {				
					String idDb = jsonNode.get("Id_ticket").asText();	
				if(idDb.equals(id)) {
					ticketDTO.setF_creacion(jsonNode.get("F_creacion").asText());
					ticketDTO.setId_ticket(jsonNode.get("Id_ticket").asText());
					ticketDTO.setTotal(jsonNode.get("Total").asText());
					ticketDTO.setDescripcion(jsonNode.get("Descripcion").asText());
					listTicketDTO.add(ticketDTO);
				}
			}
		} catch (Exception e) {
			return ResponseBuilder.error(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}			
		return ResponseBuilder.success(listTicketDTO, HttpStatus.OK);		
	}
	
	
	@Override
	public ResponseEntity<ResponseDTO> getTicketFechas(String finicio,String ffin) {
		JsonNode root = null;
		TicketDTO ticketDTO = new TicketDTO();
		ObjectMapper mapper = new ObjectMapper();
		String url = endpointDataTicket + "/goldenrace/ticket/consulta";
		System.out.println("url : "+url);
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> request = new HttpEntity<String>(headers);
		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");	
		List<TicketDTO> listTicketDTO = new ArrayList<>();		
		try {
			Date fini_pasa = formato.parse(finicio);
			Date ffin_pasa = formato.parse(ffin);			
			ResponseEntity<String> responseData = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
			String responseBody = responseData.getBody();			
			root = mapper.readTree(responseBody);
			for (JsonNode jsonNode : root) {				
				Date fecha = formato.parse(jsonNode.get("F_creacion").asText());		
				if(fecha.after(fini_pasa) && fecha.before(ffin_pasa)) {
					ticketDTO.setF_creacion(jsonNode.get("F_creacion").asText());
					ticketDTO.setId_ticket(jsonNode.get("Id_ticket").asText());
					ticketDTO.setTotal(jsonNode.get("Total").asText());
					ticketDTO.setDescripcion(jsonNode.get("Descripcion").asText());
					listTicketDTO.add(ticketDTO);
				}
			}
		} catch (Exception e) {
			return ResponseBuilder.error(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}			
		return ResponseBuilder.success(listTicketDTO, HttpStatus.OK);	
	}
	
}

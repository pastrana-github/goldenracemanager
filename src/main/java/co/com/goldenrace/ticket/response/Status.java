package co.com.goldenrace.ticket.response;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Builder;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@Builder
public class Status implements Serializable {
	
private static final long serialVersionUID = 1L;
	
	@JsonProperty(value = "StatusCode")
	@NotNull
	private Integer statusCode;
	
	@JsonProperty(value = "StatusDesc")
	@NotNull
	private String statusDesc;

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusDesc() {
		return statusDesc;
	}

	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}

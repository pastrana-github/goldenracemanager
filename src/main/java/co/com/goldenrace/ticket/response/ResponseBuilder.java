package co.com.goldenrace.ticket.response;

import java.util.Date;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import co.com.goldenrace.ticket.dto.ResponseDTO;
import co.com.goldenrace.ticket.dto.StatusResponse;

public class ResponseBuilder {

	public static ResponseDTO success() {
		return ResponseBuilder.basicResponse(0, "successfull", null);
	}

	public static ResponseDTO success(Object data) {
		return ResponseBuilder.basicResponse(0, "successfull", data);
	}

	public static ResponseDTO error(String message) {
		return ResponseBuilder.basicResponse(1, message, null);
	}

	public static ResponseDTO error(int status, String message) {
		return ResponseBuilder.basicResponse(status, message, null);
	}

	public static ResponseDTO error(Throwable error) {
		return ResponseBuilder.basicResponse(1, error.getMessage(), null);
	}

	public static ResponseDTO basicResponse(int status, String desc, Object data) {
		ResponseDTO response = new ResponseDTO();
		StatusResponse statusResponse = new StatusResponse();
		statusResponse.setStatus(status);
		statusResponse.setStatusDesc(desc);
		statusResponse.setTimestamp(new Date());
		response.setStatusResponse(statusResponse);
		response.setData(data);
		return response;
	}

	public static ResponseEntity<ResponseDTO> success(HttpHeaders responseHeaders, HttpStatus code) {
		return ResponseBuilder.responseEntity(ResponseBuilder.basicResponse(0, "successfull", null), responseHeaders, code);
	}

	public static ResponseEntity<ResponseDTO> success(Object data, HttpStatus code) {
		return ResponseBuilder.responseEntity(ResponseBuilder.basicResponse(0, "successfull", data), null, code);
	}

	public static ResponseEntity<ResponseDTO> error(String message, HttpStatus code) {
		return ResponseBuilder.responseEntity(ResponseBuilder.basicResponse(1, message, null), null, code);
	}

	public static ResponseEntity<ResponseDTO> error(int status, String message, HttpStatus code) {
		return ResponseBuilder.responseEntity(ResponseBuilder.basicResponse(status, message, null), null, code);
	}

	public static ResponseEntity<ResponseDTO> error(Throwable error, HttpStatus code) {
		return ResponseBuilder.responseEntity(ResponseBuilder.basicResponse(1, error.getMessage(), null), null, code);
	}

	private static ResponseEntity<ResponseDTO> responseEntity(ResponseDTO response, HttpHeaders responseHeaders, HttpStatus code) {
		return new ResponseEntity<ResponseDTO>(response, responseHeaders, code);
	}
}
